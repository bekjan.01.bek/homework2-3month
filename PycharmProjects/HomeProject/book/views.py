from django.http import HttpResponse
from django.shortcuts import render
from . import models

#Логика для отроброжение
def hello_world_text(request):
    return HttpResponse("Здаров как ты, как здоровье?)")


#логика для отроброжение на веб странице нашего book
def book_view(request):
    book = models.BookProgramLang.objects.all()
    return render(request, 'book.html', {'book': book})