from django.db import models


class Director(models.Model):
    name = models.CharField(max_length=312)

    class Meta:
        verbose_name = 'Режиссер'
        verbose_name_plural = 'Режиссеры'

    def __str__(self):
        return self.name


class Movie(models.Model):
    title = models.CharField('Название', max_length=312)
    description = models.TextField('Описание')
    duration = models.TimeField('Время')
    director = models.ForeignKey

    class Meta:
        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильмы'

    def __str__(self):
        return self.title


class Review(models.Model):
    text = models.CharField(max_length=312)
    movie = models.ForeignKey

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Коментарии'

    def __str__(self):
        return self.text





