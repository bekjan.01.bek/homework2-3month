from config import bot
from aiogram import types, Dispatcher


# @dp.callback_query_handler(text='button_b')
async def quiz_2(call: types.CallbackQuery):
    '''Викторина для клиентов'''
    question = "Самые популярные напитки в мире?"
    answer = [
        "Кофе",
        "Чай",
        "Кола",
        "Пиво",
        "Вода"

    ]
    await bot.send_poll(
        chat_id=call.from_user.id,
        question=question,
        options=answer,
        is_anonymous=False,
        type='quiz',
        correct_option_id=4,
        explanation="Стыдно не знать"
        # open_period=10
    )


# @dp.callback_query_handler(lambda callback_query: True)
async def button(callback_query: types.CallbackQuery):
    '''Состав меню'''
    if callback_query.data == 'button1':
        await bot.send_message(callback_query.from_user.id, 'Состав 🌯: лаваш 🫓 , говядина 🍖, помидор 🍅, '
                                                            'пикули 🥒, салат 🥬, соус 🥫 и наша любовь ❤️')
    elif callback_query.data == 'button2':
        await bot.send_message(callback_query.from_user.id, 'Состав 🌯: пите 🫓 , говядина 🍖, помидор 🍅, '
                                                            'пикули 🥒, салат 🥬, сметана, чеснок 🧄, карри 🍛 и наша любовь ❤️')
    elif callback_query.data == 'button3':
        await bot.send_message(callback_query.from_user.id, 'Состав 🌯: лаваш 🫓 , говядина 🍖, помидор 🍅,'
                                                            ' пикули 🥒, салат 🥬, соус 🥫 и наша любовь ❤️')
    elif callback_query.data == 'button4':
        await bot.send_message(callback_query.from_user.id, 'Состав 🌯: лаваш 🫓 , говядина 🍖, помидор 🍅,'
                                                            ' пикули 🥒, салат 🥬, соус 🥫, сыр 4 вида 🧀 и наша любовь ❤️')
    elif callback_query.data == 'button5':
        await bot.send_message(callback_query.from_user.id, 'Состав 🌯: лаваш 🫓 , говядина 🍖, помидор 🍅,'
                                                            ' пикули 🥒, салат 🥬, соус 🥫 + Картошка Фри 🍟 с любовь ❤️')

    await callback_query.answer()


def register_quiz_handlers(dp: Dispatcher):
    dp.register_callback_query_handler(button,lambda cb: cb.data in ['button1', 'button2', 'button3', 'button4', 'button5'])
    dp.register_callback_query_handler(quiz_2, lambda cb: cb.data == 'next_question')