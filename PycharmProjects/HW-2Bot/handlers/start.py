import os
import random

from aiogram import types
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from config import bot, dp
from handlers.constants import HELLO_TEXT


# @dp.message_handler(commands=['Hello'])
async def start_command(message: types.Message):
    '''Приветствие с пользователями'''
    await message.answer(f"Здавструйте чего изволите?) {message.from_user.full_name} ")
    await message.reply(HELLO_TEXT)


# @dp.message_handler(commands=['quiz'])
async def quiz_v(message: types.Message):
    '''Виторина для клиентов'''
    markup = InlineKeyboardMarkup()
    button = InlineKeyboardButton("NEXT", callback_data='next_question')
    markup.add(button)

    question = "От куда родом Шаурма?"
    answer = [
        "Египет",
        "Персия",
        "Османская империя",
        "Иран",
        "Карпинка"

    ]
    await bot.send_poll(
        chat_id=message.from_user.id,
        question=question,
        options=answer,
        is_anonymous=False,
        type='quiz',
        correct_option_id=2,
        explanation="Стыдно не знать",
        # open_period=10
        reply_markup=markup
    )


# @dp.message_handler(commands=['menu'])
async def menu(message: types.Message):
    '''Меню для клиентов'''
    button2 = InlineKeyboardButton("Шаурма в Пите", callback_data='button2')
    button3 = InlineKeyboardButton("Мини Шаурма", callback_data='button3')
    button4 = InlineKeyboardButton("Шаурса сырная", callback_data='button4')
    button5 = InlineKeyboardButton("Шаурма+фри", callback_data='button5')
    button1 = InlineKeyboardButton("Шаурма классическая", callback_data='button1')
    markup = types.InlineKeyboardMarkup().add(button1, button2, button3, button4, button5)

    await message.answer('Выберите то что вам Нужно 🌯🌯🌯', reply_markup=markup)


# @dp.message_handler(commands=['photo'])
async def send_menu(message: types.Message):
    '''Фото меню для клиентов'''
    photos_path = "images"
    photo_files = os.listdir(photos_path)
    random_photo = random.choice(photo_files)
    photos_path = os.path.join(photos_path, random_photo)
    with open(photos_path, 'rb') as photo_file:
        await bot.send_photo(chat_id=message.chat.id, photo=photo_file.read())


# @dp.message_handler(commands=['help'])
async def help_command(message: types.Message):
    '''О компании'''
    await message.answer(f'Здавструйте вас приветствует закусочная "Жуй глотай и не мешай". '
                         f'Тут вы испробуете самые утонченные вкусы шаурмы/шавермы,'
                         f' что о нас то мы молодая компания которая только только стала раскачиваться '
                         f'наш много уважаемый ➡️  {message.from_user.full_name} ')
