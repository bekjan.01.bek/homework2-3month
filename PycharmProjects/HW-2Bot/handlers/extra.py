from aiogram import types

from config import bot, dp


# @dp.message_handler()
async def echo(message: types.Message):
    '''Эхо ответ'''
    if message.text == 'Шаурма супер':
        await message.answer('Бесплатная шаурма вам')
    else:
        await bot.send_message(
            chat_id=message.from_user.id,
            text=f"Нажмите на меню {message.from_user.full_name}")

        await message.answer(f"А там уже выберите заказ! {message.message_id}")
        await message.reply("Бонн апетит!")
