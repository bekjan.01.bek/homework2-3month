from aiogram import types

from db.request import get_shawarma
from pprint import pprint


async def educftion(mesage: types.Message):
    kb = types.ReplyKeyboardMarkup()
    kb.add(
        types.KeyboardButton("Холодные напитки"),
        types.KeyboardButton("Горячие напитки"),
        types.KeyboardButton("Цены на шаурму"),
    )
    await mesage.answer(
        "Выберите что будете заказывать",
        reply_markup=kb
    )


async def shawarma_all (messages: types.Message):
    ''' Информирует цены на них'''
    shawarma = get_shawarma()
    shawarma = list(shawarma)
    pprint(shawarma)
    c = 1
    for i in shawarma:
        await messages.answer(f'{c}) {i[0]} {i[1]} цена: {i[2]} сом')
        c += 1


async def cold_drinks(message: types.Message):
    await message.answer(f"Кока-Кола 🥤 70 сом,\n "
                         f"Фанта 🥤 70 сом,\n "
                         f"Спрайт 🥤70 сом,\n"
                         f"Натуральные Соки 🧃 70 сом,\n"
                         f"Максым 🍶 90 сом")


async def hot_drinks(message: types.Message):
    await message.answer(f"Чай 🍵 40 сом,\n "
                         f"Кофе ☕ 70 сом,\n "
                         f"Безалкогольный глинтвейн 🍹 120 сом,\n"
                         f"Горячий шоколад ☕+🍫 70 сом")



