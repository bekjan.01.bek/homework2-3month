import logging

from aiogram.dispatcher.filters import Text
from aiogram.utils import executor
from config import dp, scheduler
from handlers.callback import register_quiz_handlers
from handlers.education import cold_drinks, educftion, hot_drinks, shawarma_all
from handlers.extra import echo
from handlers.start import help_command, menu, send_menu, start_command, quiz_v
from handlers.user_form import register_survey_handlers
from db.request import init_db, drop_tables, create_tables, populate_tables
#from handlers.notifications import notify
from handlers.group_admin import catch_bad_words




async def bot_start(_):
    init_db()
    drop_tables()
    create_tables()
    populate_tables()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    dp.register_message_handler(start_command, commands=['hello'])
    dp.register_message_handler(quiz_v, commands=['quiz'])
    dp.register_message_handler(menu, commands=['menu'])
    dp.register_message_handler(send_menu, commands=['photo'])
    dp.register_message_handler(help_command, commands=['help'])


    register_quiz_handlers(dp)



    dp.register_message_handler(educftion, commands=['price'])
    dp.register_message_handler(cold_drinks, Text("Холодные напитки"))
    dp.register_message_handler(hot_drinks, Text("Горячие напитки"))
    dp.register_message_handler(shawarma_all, Text("Цены на шаурму"))

    # планировщик
    #dp.register_message_handler(notify)

    #Форма
    register_survey_handlers(dp)

    #The end
    dp.register_message_handler(catch_bad_words)

    scheduler.start()

    executor.start_polling(
        dp,
        on_startup=bot_start,
        skip_updates=True
    )