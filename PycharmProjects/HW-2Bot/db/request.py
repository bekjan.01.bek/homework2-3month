import sqlite3
from pathlib import Path


def init_db():
    global db, cursor
    DB_PATH = Path(__file__).parent.parent
    DB_NAME = 'db.sqlite'
    db = sqlite3.connect(DB_PATH/DB_NAME)
    cursor = db.cursor()


def create_tables():
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS shawarma(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    title TEXT,
    price INTEGER
    )
    ''')
    db.commit()

    cursor.execute('''
    CREATE TABLE IF NOT EXISTS user_resaults(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    age INTEGER,
    gender TEXT
    )
    ''')
    db.commit()

    db.commit()




def populate_tables():
    cursor.execute('''
    INSERT INTO shawarma(name, title, price)
    VALUES ("Шаурма", "Классическая в лаваше", "190"),
            ("Шаурма", "в Пите", "180"),
            ("Шаурма", "Мини", "150"),
            ("Шаурма", "Сырная", "220"),
            ("Шаурма", "На тарелке + ФРИ", "250")

    ''')
    db.commit()

def save_user_results(data):
    cursor.execute("""
    INSERT INTO user_resaults(name, age, gender)
    VALUES (:name, :age, :gender)
    """,
                   {'name': data['name'],
                    'age': data['age'],
                    'gender': data['gender']}
                   )
    db.commit()



def drop_tables():
    cursor.execute('''
    DROP TABLE IF EXISTS shawarma
    ''')
    db.commit()


def get_shawarma():
    shawarma = cursor.execute('''
    SELECT name, title, price FROM shawarma;
    ''')
    return shawarma.fetchall()


if __name__ == "__main__":
    init_db()
    drop_tables()
    create_tables()
    populate_tables()




#
# def get_tovar():
#     tovar = cursor.execute("""
#     SELECT name , title , price FROM tovar;
#     """)
#     return tovar.fetchall()
#
